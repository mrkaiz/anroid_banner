package com.example.admin.mybanner;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Scroller;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * 程序主入口
 * @author admin
 */
public class MainActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener {

    private ViewPager viewPager;
    private LinearLayout ll_point_container;
    private String[] contentDescs;
    private TextView tv_desc;
    private int previousSelectedPosition = 0;
    private boolean isRunning = false;
    private int[] imageResIds;
    private View backView;
    private TextView name;
    private List<Drawable> blurImg = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // 初始化布局 View视图
        initViews();
        // Model数据
        initData();
        // Controller 控制器
        initAdapter();
        // 滚动线程
        autoPlay();
    }

    private void autoPlay() {
        // 开启轮询
        new Thread() {
            public void run() {
                isRunning = true;
                while (isRunning) {
                    try {
                        Thread.sleep(3000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    // 往下跳一位
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
                        }
                    });
                }
            }
        }.start();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isRunning = false;
    }

    private void initViews() {
        viewPager = findViewById(R.id.viewpager);
        // 设置页面更新监听
        viewPager.addOnPageChangeListener(this);
        ll_point_container = findViewById(R.id.ll_point_container);
        tv_desc = findViewById(R.id.tv_desc);
        backView = findViewById(R.id.backView);
        name = findViewById(R.id.name);
    }

    /**
     * 初始化要显示的数据
     */
    private void initData() {
        // 图片资源id数组
        imageResIds = new int[]{R.mipmap.banner1, R.mipmap.banner2, R.mipmap.banner3, R.mipmap.banner4, R.mipmap.banner5};
        // 文本描述
        contentDescs = new String[]{"1", "2", "3", "4", "5"};
        View pointView;
        LinearLayout.LayoutParams layoutParams;
        for (int i = 0; i < imageResIds.length; i++) {
            // 加小白点, 指示器
            pointView = new View(this);
            pointView.setBackgroundResource(R.drawable.selector_bg_point);
            layoutParams = new LinearLayout.LayoutParams(20, 20);
            if (i != 0) {
                layoutParams.leftMargin = 10;
            }
            // 设置默认所有都不可用
            pointView.setEnabled(false);
            ll_point_container.addView(pointView, layoutParams);

//            Resources res = getResources();
//            Bitmap mBitmap = BitmapFactory.decodeResource(res, imageResIds[i]);
//            Bitmap newBitmap = StackBlur.blur(mBitmap, 100, false);
//            Drawable drawable = new BitmapDrawable(newBitmap);
//            blurImg.add(drawable);
        }
        name.setText("修改成功");
    }
    private void initAdapter() {
        ll_point_container.getChildAt(0).setEnabled(true);
        tv_desc.setText(contentDescs[0]);
        previousSelectedPosition = 0;
        // 设置适配器
        viewPager.setAdapter(new MyAdapter(this, imageResIds));
        // 默认设置到中间的某个位置
        int content = Integer.MAX_VALUE / 2;
        viewPager.setCurrentItem(content - content % imageResIds.length);
        //显示viewpager间距
        viewPager.setPageMargin(20);
        viewPager.setOffscreenPageLimit(3);
        //切换动画
        viewPager.setPageTransformer(false, new DepthPageTransformer());
        //这个是设置切换过渡时间为2秒
        ViewPagerScroller scroller = new ViewPagerScroller(this);
        scroller.setScrollDuration(2000);
        scroller.initViewPagerScroll(viewPager);
    }


    class MyAdapter extends PagerAdapter {
        private final Context mContext;
        private final int[] mList;

        MyAdapter(Context context, int[] list) {
            this.mContext = context;
            this.mList = list;
        }

        @Override
        public int getCount() {
            return Integer.MAX_VALUE;
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return view == object;
        }

        // 1. 返回要显示的条目内容, 创建条目
        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, final int position) {
            int newPosition = position % imageResIds.length;
            View view = LayoutInflater.from(mContext).inflate(R.layout.item, container, false);
            ImageView imageView = view.findViewById(R.id.img);
            imageView.setImageResource(mList[newPosition]);
            imageView.setScaleType(ImageView.ScaleType.CENTER);
            container.addView(imageView);
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(mContext, "点击了图片" + position % imageResIds.length, Toast.LENGTH_SHORT).show();
                }
            });
            return imageView;
        }

        // 2. 销毁条目
        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            // object 要销毁的对象
            container.removeView((View) object);
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        // 滚动时调用
    }

    @Override
    public void onPageSelected(int position) {
        // 新的条目被选中时调用
        int newPosition = position % imageResIds.length;
        //设置文本
        tv_desc.setText(contentDescs[newPosition]);
        // 把之前的禁用, 把最新的启用, 更新指示器
        ll_point_container.getChildAt(previousSelectedPosition).setEnabled(false);
        ll_point_container.getChildAt(newPosition).setEnabled(true);
//        backView.setBackgroundDrawable(blurImg.get(newPosition));
//        backView.setBackgroundResource(imageResIds[newPosition]);
        // 记录之前的位置
        previousSelectedPosition = newPosition;
    }

    @Override
    public void onPageScrollStateChanged(int state) {
        // 滚动状态变化时调用
    }

    class DepthPageTransformer implements ViewPager.PageTransformer {
        private static final float MIN_SCALE = 0.9f;
        private static final float MIN_ALPHA = 1f;

        @Override
        public void transformPage(@NonNull View view, float position) {
            if (position < -1) {
                // [-Infinity,-1)
                // This page is way off-screen to the left.
                view.setScaleY(MIN_SCALE);
                view.setAlpha(MIN_ALPHA);
            } else if (position == 0) { // [-1,0]
                // Use the default slide transition when moving to the left page
                view.setScaleY(1);
                view.setAlpha(1);
            } else if (position <= 1) { // (0,1]
                // Fade the page out.
                // Counteract the default slide transition
                // Scale the page down (between MIN_SCALE and 1)
                float scaleFactor = MIN_SCALE + (1 - MIN_SCALE) * (1 - Math.abs(position));
                float alphaFactor = MIN_ALPHA + (1 - MIN_ALPHA) * (1 - Math.abs(position));
                view.setScaleY(scaleFactor);
                view.setAlpha(alphaFactor);
            } else {
                // (1,+Infinity]
                // This page is way off-screen to the right.
                view.setScaleY(MIN_SCALE);
                view.setAlpha(MIN_ALPHA);
            }
        }
    }

    class ViewPagerScroller extends Scroller {
        private int mScrollDuration = 2000;             // 滑动速度

        @SuppressWarnings("SameParameterValue")
        void setScrollDuration(int duration) {
            this.mScrollDuration = duration;
        }

        ViewPagerScroller(Context context) {
            super(context);
        }

        @Override
        public void startScroll(int startX, int startY, int dx, int dy, int duration) {
            super.startScroll(startX, startY, dx, dy, mScrollDuration);
        }

        @Override
        public void startScroll(int startX, int startY, int dx, int dy) {
            super.startScroll(startX, startY, dx, dy, mScrollDuration);
        }


        void initViewPagerScroll(ViewPager viewPager) {
            try {
                Field mScroller = ViewPager.class.getDeclaredField("mScroller");
                mScroller.setAccessible(true);
                mScroller.set(viewPager, this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}